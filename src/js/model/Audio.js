/**
 * @class Audio
 * @description Clase que representa a un audio.
 *
 */


export class Audio {

    id;
    name;
    duration;

    constructor(name,duration,id) {
        this.name = name;
        this.duration = duration;
        this.id = id;
    }

    getName() {
        return this.name;
    }

    setName(value) {
        this.name = value;
    }

    getDuration() {
        return this.duration;
    }

    setDuration(value) {
        this.duration = value;
    }

    getID() {
        return this.id;
    }

    setID(value) {
        this.id = value;
    }
}