/**
 * @class PlayList
 * @description Clase que representa a una PlayList
 *
 */

import {Song} from "./Song.js";

export class PlayList {

    id;
    name;
    image;
    songs = [];
    description;

    constructor(id,name,image,songs,description) {
        this.id = id;
        this.name = name;
        this.image = image;
        this.songs = songs;
        this.description = description;
    }

    getID() {
        return this.id;
    }

    setID(value) {
        this.id = value;
    }

    getName() {
        return this.name;
    }

    setName(value) {
        this.name = value;
    }

    getImage() {
        return this.image;
    }

    setImage(value) {
        this.image = value;
    }

    getSongs() {
        return this.songs;
    }

    setSongs(value) {
        this.songs = value;
    }

    getDescription() {
        return this.description;
    }

    setDescription(value) {
        this.description = value;
    }

}