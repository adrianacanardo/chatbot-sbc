/**
 * @class Singer
 * @description Clase que representa a un cantante.
 *
 */

import {Artist} from "./Artist.js";

export class Singer extends Artist {

    spotifyID;
    albums;
    songs;

    constructor(name,age,id, albums,songs) {
        super(name,age);
        this.spotifyID = id;
        this.albums = albums;
        this.songs = songs;
    }

    getSpotifyID() {
        return this.spotifyID;
    }

    setSpotifyID(value) {
        this.spotifyID = value;
    }

    getAlbums() {
        return this.albums;
    }

    setAlbums(value) {
        this.albums = value;
    }

    getSongs() {
        return this.songs;
    }

    setSongs(value) {
        this.songs = value;
    }
}