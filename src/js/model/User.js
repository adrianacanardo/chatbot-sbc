/**
 * @class User
 * @description Clase que representa a un usuario.
 *
 */


export class User {

    ID;
    displayName;
    image;

    constructor(ID,displayName, image) {
        this.ID = ID;
        this.displayName = displayName;
        this.image = image;
    }

    getID() {
        return this.ID;
    }

    setID(value) {
        this.ID = value;
    }

    getDisplayName() {
        return this.displayName;
    }

    setDisplayName(value) {
        this.displayName = value;
    }

    getImage() {
        return this.image;
    }

    setImage(value) {
        this.image = value;
    }

}