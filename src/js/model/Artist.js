/**
 * @class Artist
 * @description Clase que representa a un artista.
 *
 */

export class Artist {
    name;
    age;
    nationality;
    nationCode;
    image;
    genres = [];

    constructor(name,age) {
        this.name = name;
        this.age = age;
        this.nationality = "";
        this.nationCode = "";
    }

    getImg(){
        return this.image;
    }

    setImg(img){
        this.image = img;
    }

    setNationCode(value){
        this.nationCode = value;
    }

    getNationCode(){
        return this.nationCode;
    }

    getNationality() {
        return this.nationality;
    }

    setNationality(value) {
        this.nationality = value;
    }

    getName() {
        return this.name;
    }

    setName(value) {
        this.name = value;
    }

    getAge() {
        return this.age;
    }

    setAge(value) {
        this.age = value;
    }

    getGenres() {
        return this.genres;
    }

    setGenres(value) {
        this.genres = value;
    }

}