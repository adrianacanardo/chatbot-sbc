/**
 * @class Song
 * @description Clase que representa a una canción.
 *
 */
import {Audio} from "./Audio.js";
import {Album} from "./Album.js";

export class Song extends Audio {

    album;
    genre;
    lyrics;
    trackNumber;


    constructor(name,duration,id,genre,lyrics,album) {
        super(name,duration,id);
        this.genre = genre;
        this.lyrics = lyrics;
    }

    getGenre() {
        return this.genre;
    }

    setGenre(value) {
        this.genre = value;
    }

    getLyrics() {
        return this.lyrics;
    }

    setLyrics(value) {
        this.lyrics = value;
    }

    getAlbum() {
        return this.album;
    }

    setAlbum(value) {
        this.album = value;
    }

    getTrackNumber() {
        return this.trackNumber;
    }

    setTrackNumber(value) {
        this.trackNumber = value;
    }

}