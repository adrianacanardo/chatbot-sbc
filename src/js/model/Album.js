/**
 * @class Album
 * @description Clase que representa a un album.
 *
 */


export class Album {

    spotifyID;
    name;
    singer;
    songs = [];
    releaseDate;
    totalTracks;
    image;

    constructor(name,singer,songs, releaseDate, totalTracks, spotifyID) {
    }

    getSpotifyID() {
        return this.spotifyID;
    }

    setSpotifyID(value) {
        this.spotifyID = value;
    }

    getTotalTracks() {
        return this.totalTracks;
    }

    setTotalTracks(value) {
        this.totalTracks = value;
    }

    getImage() {
        return this.image;
    }

    setImage(value) {
        this.image = value;
    }

    getReleaseDate() {
        return this.releaseDate;
    }

    setReleaseDate(value) {
        this.releaseDate = value;
    }

    getName() {
        return this.name;
    }

    setName(value) {
        this.name = value;
    }

    getSinger() {
        return this.singer;
    }

    setSinger(value) {
        this.singer = value;
    }

    getSongs() {
        return this.songs;
    }

    setSongs(value) {
        this.songs = value;
    }
}