/**
 * @class Bot
 * @description Clase que representa la lógica del bot.
 */

import * as api from './api.js';
import {Album} from "./model/Album.js";
import * as stringSimilarity from "./utils/string-similarity.js";

// Define type of questions:
const STRING_VALUE = "stringValue";
const NUMBER_VALUE = "numberValue";
// SONG & ALBUM
const DURATION_ALBUM_SONG = "durationAlbumOrSong";
const DATE_ALBUM_SONG = "dateAlbumOrSong";
// ALBUM
const WHO_ALBUM = "whoAlbum";
const SONGS_IN_ALBUM = "songsInAlbum";
const ALBUM_INFO = "infoAlbum";
// ARTIST
const WHERE_ARTIST_FROM = "whereIsArtistFrom";
const SIMILAR_ARTISTS = "similarArtists";
const AGE_ARTIST = "ageArtist";
const ALBUMS_ARTIST = "albumsOfArtist";
// PLAYLISTS
const MY_PLAYLISTS = "myPlaylists";
const ADD_SONG_PLAYLIST = "addSongToPlaylist";
// PLAY SONG
const PLAY_SONG = "playSong";
const NEXT_SONG = "nextSong";
const BEFORE_SONG = "beforeSong";
// RECOMMENDATIONS
const RECOMMEND_SONG_ARTIST = "recommendSongArtist";
const RECOMMEND_GENRE = "recommendGenre"
//LYRICS
const LYRICS_SONG = "lyricsSong";
// HELP
const HELP = "help";
const ERROR = "Error";
const DIALOGFLOW_RESPONSE = 1;



/**
 * Método encargado de encontrar una posible respuesta para el bot.
 *
 * @param response Respuesta recibida por parte de DialogFlow con su procesamiento de NLP.
 * @return ---
 */
export async function getResponse(response) {

    // Si se ha encontrado un tipo de Query
    const queryType = getResponseQueryType(response);

    switch(queryType) {

        // *** ALBUM RELATED QUESTIONS *** //
        case ALBUM_INFO: {

            const albumName = getResponseValue(response,STRING_VALUE,'title');
            if(albumName) {
                const album = await api.searchAlbum(albumName);
                if(album !== ERROR) {
                    return getAllInformationAlbum(album);
                } else {
                    return "Lo siento, no he podido encontrar información sobre el álbum: '"+ albumName + "'";
                }
            }
            break;
        }

        case WHO_ALBUM: {
            const albumName = getResponseValue(response,STRING_VALUE,'title');
            if(albumName) {
                const album = await api.searchAlbum(albumName);

                if(album !== ERROR) {
                    return "El álbum '" + album.getName() + "' pertenece a " + album.getSinger().getName() + ".";
                } else {
                    return "Lo siento, no he podido encontrar información del álbum: '"+ albumName + "'";
                }
            }
            break;
        }

        case SONGS_IN_ALBUM: {
            const albumName = getResponseValue(response,STRING_VALUE,'title');
            if(albumName) {
                const album = await api.searchAlbum(albumName);
                if(album !== ERROR) {
                    return getSongsAlbum(album);
                } else {
                    return "Lo siento, no he podido encontrar información sobre las canciones del álbum: '"+ albumName + "'";
                }
            }
            break;
        }

        case DURATION_ALBUM_SONG: {

            const albumOrSong = getResponseValue(response,STRING_VALUE,'albumOrSong');
            const title = getResponseValue(response,STRING_VALUE,'title');

            if(title && albumOrSong) {

                if(albumOrSong == "album") {
                    const albumName = title;
                    const album = await api.searchAlbum(albumName);
                    if(album !== ERROR) {
                        return "El álbum '" + album.getName() + " de " + album.getSinger().getName() + " dura en total: " + getAlbumDuration(album) + " min";
                    } else {
                        return "Lo siento, no he podido encontrar información sobre la duración del álbum: '"+ albumName + "'";
                    }
                } else {

                    const songName = title;
                    let song = await api.searchSong(songName);
                    console.log(song);

                    if(song != ERROR) {
                        return "La canción '" + song.getName() + "' de " + song.getAlbum().getSinger().getName() + " dura en total: " + getDuration(song.getDuration()) + " min";
                    } else {
                        return "Lo siento, no he podido encontrar información sobre la duración de la canción: '"+ songName + "'";
                    }
                }
            }
            break;
        }

        case DATE_ALBUM_SONG: {
            const albumOrSong = getResponseValue(response,STRING_VALUE,'albumOrSong');
            const title = getResponseValue(response,STRING_VALUE,'title');
            if(title && albumOrSong) {

                if(albumOrSong == "album") {
                    const albumName = title;
                    const album = await api.searchAlbum(albumName);
                    if(album !== ERROR) {
                        return "El álbum '" + album.getName() + " de " + album.getSinger().getName() + " salió en: " + album.getReleaseDate() + ".";
                    } else {
                        return "Lo siento, no he podido encontrar información sobre la fecha de lanzamiento del álbum: '"+ albumName + "'";
                    }
                } else {
                    const songName = title;
                    const song = await api.searchSong(songName);

                    if(song !== ERROR) {
                        return "La canción '" + song.getName() + " de " + song.getAlbum().getSinger().getName() + " salió en: " + song.getAlbum().getReleaseDate() + ".";
                    } else {
                        return "Lo siento, no he podido encontrar información sobre la fecha de lanzamiento de la canción: '"+ songName + "'";
                    }
                }
            }
            break;
        }

        // *** SINGER RELATED QUESTIONS *** //
        case ALBUMS_ARTIST: {
            const musicArtist = getResponseValue(response,STRING_VALUE,'music-artist');
            if(musicArtist) {
                return reqAlbumsFromArtistAndBeautify(musicArtist);
            }
            break;
        }

        case AGE_ARTIST: {
            const musicArtist = getResponseValue(response,STRING_VALUE,'music-artist');
            if(musicArtist) {
                return reqArtistAge(musicArtist);
            }
            break;
        }

        case SIMILAR_ARTISTS: {
            const musicArtist = getResponseValue(response,STRING_VALUE,'music-artist');
            if(musicArtist) {
                return reqForRelated(musicArtist);
            }
            break;
        }

        case WHERE_ARTIST_FROM: {
            let musicArtist = getResponseValue(response,STRING_VALUE,'music-artist');
            if(musicArtist) {
                return reqArtistNationality(musicArtist);
            }
            break;
        }

        // *** PLAYLISTS RELATED QUESTIONS *** //
        case MY_PLAYLISTS: {

            let playlists = await api.searchMyPlayLists();
            if (!(playlists === "Error")) {
                return getPlayLists(playlists);
            } else {
                return "Lo siento, no he podido encontrar ninguna PlayList creada";
            }
            break;
        }

        case ADD_SONG_PLAYLIST: {

            const song = getResponseValue(response,STRING_VALUE,'song');
            const playlist = getResponseValue(response,STRING_VALUE,'playlist');

            if(song && playlist) {
                let message = await api.addSongToMyPlayList(song,playlist);
                return message + "<br><br>" + response[DIALOGFLOW_RESPONSE];
            }
            break;
        }

        // *** PLAYER RELATED QUESTIONS *** //
        case PLAY_SONG: {
            const song = getResponseValue(response,STRING_VALUE,'song');
            if(song) {
                return await playSong(song);
            }
            break;
        }

        case NEXT_SONG: {
            return await nextOrBeforeSong(NEXT_SONG);
        }

        case BEFORE_SONG: {
            return await nextOrBeforeSong(BEFORE_SONG);
        }

        // *** LYRICS *** //
        case LYRICS_SONG: {
            let song = getResponseValue(response,STRING_VALUE,'song');

            // Miramos que el usuario haya introducido una canción.
            if(song) {
                let songAndArtist = await api.getSongAndArtist(song);   // Obtenemos nombre correcto y cantante de la canción.

                if(songAndArtist !== "ERROR") {
                    // Parseamos canción y artista.
                    let songN = songAndArtist[0];
                    let artistN = songAndArtist[1];
                    // Obtenemos letra de la canción.

                    let lyricsSong = await requestLyrics(songN + " " + artistN);

                    if(lyricsSong !== "ERROR") {

                        // Eliminamos primeras frases de la respuesta.
                        let index = lyricsSong.indexOf("\n");
                        lyricsSong =  lyricsSong.substring(index+1);

                        lyricsSong = "Aquí tienes la letra de la canción " + songN.toUpperCase() + " de " + artistN +":\n\n" + lyricsSong;
                        return lyricsSong.replace(/\n/g, "<br/>");

                    } else {
                        return "No he podido encontrar información de la canción: '" + songN + "'";
                    }
                } else {
                    return "No he podido encontrar información de la canción: '" + song + "'";
                }
            }
            break;
        }

        // *** RECOMMENDATION RELATED QUESTIONS *** //
        case RECOMMEND_SONG_ARTIST: {

            const artist = getResponseValue(response,STRING_VALUE,'music-artist');
            const firstYear = getResponseValue(response,NUMBER_VALUE,'firstYear');
            const finalYear = getResponseValue(response,NUMBER_VALUE,'finalYear');
            return recommendSongArtist(artist,firstYear,finalYear);
            break;
        }

        case RECOMMEND_GENRE: {
            const genre = getResponseValue(response,STRING_VALUE,'genero');
            const firstYear = getResponseValue(response,NUMBER_VALUE,'firstYear');
            const finalYear = getResponseValue(response,NUMBER_VALUE,'finalYear');

            if(genre) return recommendGenre(genre,firstYear,finalYear);
            break;
        }

        case HELP: {
            return getHelpCommand();
            break;
        }
    }
    return response[DIALOGFLOW_RESPONSE];
}

/**
 * Método encargado de obtener el códido de un PlayButton para mostrar por pantalla asociado a una canción de Spotify.
 *
 * @param songID ID de la canción a poder reproducir.
 *
 * @return Playbutton que reproduce la canción indicada.
 */
function getPLayButtonHTML(songID) {

    return `
        <div id="${songID}" class="playButton">
         <svg id="play"  viewBox="0 0 163 163" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px"="0px">
            <g fill="none">
                <g  transform="translate(2.000000, 2.000000)" stroke-width="4">
                    <path d="M10,80 C10,118.107648 40.8923523,149 79,149 L79,149 C117.107648,149 148,118.107648 148,80 C148,41.8923523 117.107648,11 79,11" id="lineOne" stroke="#A5CB43"></path>
                    <path d="M105.9,74.4158594 L67.2,44.2158594 C63.5,41.3158594 58,43.9158594 58,48.7158594 L58,109.015859 C58,113.715859 63.4,116.415859 67.2,113.515859 L105.9,83.3158594 C108.8,81.1158594 108.8,76.6158594 105.9,74.4158594 L105.9,74.4158594 Z" id="triangle" stroke="#A3CD3A"></path>
                    <path d="M159,79.5 C159,35.5933624 123.406638,0 79.5,0 C35.5933624,0 0,35.5933624 0,79.5 C0,123.406638 35.5933624,159 79.5,159 L79.5,159" id="lineTwo" stroke="#A5CB43"></path>
                </g>
            </g>
        </svg>\
        </div> 
        `
}

/**
 * Método encargado de recomendar una canción por género.
 *
 * @param genre Nombre del género del que buscar la canción.
 * @param firstYear Primer año del que filtrar.
 * @param finalYear Último año del que filtrar o el único año del que filtrar.
 *
 * @return {string} Información de la canción y PlayButton de la misma.
 */
async function recommendGenre(genre, firstYear, finalYear) {

    if(genre !== '') {
        let song = await api.recommendSongGenre(genre, firstYear, finalYear);

        if(song != ERROR) {
            return `
                <div class="text-and-play">
                     <div>
                     Te recomiendo la canción: 
                     <br>
                     <p style="display:inline" class="album-info-title-font">${song.getName()}</p> de 
                     <p style="display:inline" class="album-info-title-font">${song.getAlbum().getSinger().getName()}</p> publicada en 
                     <p style="display:inline" class="album-info-title-font">${song.getAlbum().getReleaseDate()}</p>
                     </div>
                     <div>${getPLayButtonHTML(song.getID())}</div>
                </div>
                 `
        } else {
            return "No he podido encontrar una canción con las características que me has pedido :(";
        }
    }
}

/**
 * Método encargado de recomendar una canción por artista.
 *
 * @param artist Nombre del artista del que buscar la canción.
 * @param firstYear Primer año del que filtrar.
 * @param finalYear Último año del que filtrar o el único año del que filtrar.
 *
 * @return {string} Información de la canción y PlayButton de la misma.
 */
async function recommendSongArtist(artist, firstYear, finalYear) {

    let song = await api.recommendSongArtist(artist,firstYear,finalYear);


    if(song != ERROR) {
        return `
                <div class="text-and-play">
                     <div>
                     Te recomiendo la canción: 
                     <br>
                     <p style="display:inline" class="album-info-title-font">${song.getName()}</p> de 
                     <p style="display:inline" class="album-info-title-font">${song.getAlbum().getSinger().getName()}</p> publicada en 
                     <p style="display:inline" class="album-info-title-font">${song.getAlbum().getReleaseDate()}</p>
                     </div>
                     <div>${getPLayButtonHTML(song.getID())}</div>
                </div>
                 `
    } else {
        return "No he podido encontrar una canción con las características que me has pedido :(";
    }
}

/**
 * Método encargado de obtener toda la informació de un álbum para mostrarla por pantalla.
 *
 * @param {Album} album Álbum del que extraer la información.
 * @return {string} Información del álbum para mostrar por pantalla.
 */
function getAllInformationAlbum(album) {

    let genres = "";
    let i = 0;
    album.getSinger().getGenres().forEach(genre => {

        if(i > 0) {
            genres += ", " + genre;
        } else {
            genres += genre;
        }
        i++;
    })

    let songsHTML = "";
    album.getSongs().forEach(song => {
        songsHTML = songsHTML + (`
           <div id = "information" class = "flex">
                ${getPLayButtonHTML(song.getID())}
                <div class="songTitle-text">
                    <a href="https://open.spotify.com/track/${song.getID()}" class="text-album-other" target="_blank">
                        ${album.getSongs().indexOf(song)+1} ${song.getName()}  (${getDuration(song.getDuration())})
                    </a>
                </div>
           </div>`
        );
    });

    let result = `
                    Aquí tienes información del álbum:
                    <div class="album-info">
                        <div class="album-image-info">
                            <div class="album-image-info-image">
                            <a href="https://open.spotify.com/album/${album.getSpotifyID()}" target="_blank">
                                <img src=${album.getImage()} alt="imagen-album">
                            </a>
                              
                            </div>
                            <div class="album-image-info-info">
                                <p class="album-info-title-font">
                                    ${album.getName()} -
                                    ${album.getSinger().getName()} 
                                </p>
                                <p class="text-album-other">
                                    Géneros: ${genres}
                                    <br>
                                    Lanzamiento: ${album.getReleaseDate()}
                                    <br>
                                    Canciones: ${album.getTotalTracks()}
                                </p>
                            </div>
                        </div>
                        <div class="album-songs-info">
                            <p class="album-info-title-font">
                                Canciones:
                            </p>`
        +
        songsHTML;
        +
        `<div class="album-songs-info">
                            <p class="album-info-title-font">
                                Canciones:
                            </p>
                            <p class="text-album-other">
                                Titulo + (03:45)
                            </p>
                        </div>
                    </div>`
    return result;
}

/**
 * Método encargado de mostrar las canciones que contiene un álbum.
 *
 *  @param {Album} album Álbum del que extraer la información.
 *  @return {string} Canciones del álbum a mostrar por pantalla.
 *
 */
function getSongsAlbum(album) {

    let songsHTML = "";
    album.getSongs().forEach(song => {
        songsHTML = songsHTML + (`
      
            <a href="https://open.spotify.com/track/${song.getID()}" class="text-album-other" target="_blank">
                ${album.getSongs().indexOf(song)+1} ${song.getName()}  (${getDuration(song.getDuration())})
            </a>
            <br>`
        );
    });

    let result = `
    <div class="album-info">
        <div class="album-songs-info">
            <p class="album-info-title-font">
                Canciones:
            </p>`
        +
        songsHTML;
        +
        `<div class="album-songs-info">
            <p class="album-info-title-font">
                Canciones:
            </p>
            <p class="text-album-other">
                Titulo + (03:45)
            </p>
        </div>
    </div>`
    return "El álbum '" + album.getName() + " de " + album.getSinger().getName() +" tiene las siguientes canciones: " + result;
}

/**
 * Método encargado de mostrar las canciones que contiene un álbum.
 *
 *  @param {Album} playlists  del que extraer la información.
 *  @return {string} Canciones del álbum a mostrar por pantalla.
 *
 */
function getPlayLists(playlists) {

    let playlistsHTML = "";
    playlists.forEach(playlist => {
        playlistsHTML = playlistsHTML + (`
            <p class="text-album-other">
                ${playlists.indexOf(playlist)+1} ${playlist.getName()}
            </p>`
        );
    });

    let result = `
    <div class="album-info">
        <div class="album-songs-info">
            <p class="album-info-title-font">
                PlayLists:
            </p>`
        +
        playlistsHTML;
    return "Tienes las siguientes playlists almacenadas: " + result;
}

/************************************************************************************
 *
 * @Objetivo: Método encargado de encontrar artistas relacionados con un artista en concreto.
 *
 * @Parameters:
 *              (in) artistN: Artista del cual buscar los álbumes.
 *
 * @Return:     Mensaje a mostrar en formato HTML
 *
 ************************************************************************************/
async function reqForRelated(artistN) {
    let artistsList = await api.getArtistsRelatedToArtist(artistN);
    let artistsHTML = "";
    var index = 1;
    artistsList.forEach(artist =>{
        artistsHTML = artistsHTML + (`
            <div class="h${index}jn related-artist">
                <div class="artist-rel-img">
                    <a href="https://open.spotify.com/artist/${artist.getSpotifyID()}" target="_blank">
                     <img src="${artist.getImg()}" alt="" width="90px" height="90px">
                    </a>
                </div>
                <p>${artist.getName()}</p>
            </div>`
        );
        index++;
    });

    return `
        <p>Aquí tienes un conjunto de artistas relacionados con: <b>${artistN}</b></p><br>
        <div class="rel-artists-grid">`
        +
        artistsHTML
        +
        `
        </div>
        <br><br><p>En que más puedo ayudarte? 😄</p>
        `;
}

/************************************************************************************
 *
 * @Objetivo: Método encargado de encontrar los albumes de un artista y generar el html
 *      correspondiente para que se muestre por pantalla.
 *
 * @Parameters:
 *              (in) artistN: Artista del cual buscar los álbumes.
 *
 * @Return:     Mensaje a mostrar en formato HTML
 *
 ************************************************************************************/
async function reqAlbumsFromArtistAndBeautify(artistN){

    try {
        let albumsObj = await api.getAlbumsFromArtist(artistN, 30);
        let newestAlbum;

        function dateCompare(a1, a2){
            const date1 = new Date(a1.getReleaseDate());
            const date2 = new Date(a2.getReleaseDate());

            if(date1 < date2){
                newestAlbum = a2;
            }
        }

        let albumsHTML = "";
        newestAlbum = albumsObj[0];
        let i = 0;
        albumsObj.forEach(album => {
            dateCompare(newestAlbum, album);
            albumsHTML = albumsHTML + (`
            <a href="https://open.spotify.com/album/${album.spotifyID}" class="text-album-other" target="_blank">
                ${i+1}. ${album.getName()} (${album.getReleaseDate()})
            </a>
            <br>`

            );
            i++;
        });


        let result = `
                    Los álbumes de ${artistN} son:
                    <div class="album-info">
                        <div class="album-image-info">
                            <p>Último lanzamiento:<br></p>
                            <div class="album-image-info-image">
                                <img src=${newestAlbum.getImage()} alt="imagen-album">
                            </div>
                            <div class="album-image-info-info">
                                <p class="album-info-title-font">
                                    ${newestAlbum.getName()} -
                                    ${newestAlbum.getSinger().getName()} 
                                </p>
                                <p class="text-album-other">
                                    Lanzamiento: ${newestAlbum.getReleaseDate()}
                                    <br>
                                    Canciones: ${newestAlbum.getTotalTracks()}
                                </p>
                            </div>
                        </div>
                        <div class="album-songs-info">
                            <p class="album-info-title-font">
                                Álbumes (máx. 30):
                            </p>`
            +
            albumsHTML;
        +
            `<div class="album-songs-info">
                            <p class="album-info-title-font">
                                Álbumes:
                            </p>
                            <p class="text-album-other">
                                Titulo + (Fecha de lanzamiento)
                            </p>
                        </div>
                    </div>`

        return result;
    } catch (e) {
        return "No he podido encontrar los álbumes del artista introducido. Prueba comprobar el nombre del artista.";
    }



}

/************************************************************************************
 *
 * @Objetivo: Método encargado de buscar la nacionalidad de un artista.
 *
 * @Parameters:
 *              (in) artistN: Artista a buscar.
 *
 * @Return:     Mensaje a mostrar
 *
 ************************************************************************************/
async function reqArtistNationality(artistN){
    try{
        let artistObj = await api.getArtistInformation(artistN);
        return artistObj.strArtist + " es de " + artistObj.strCountry.split(", ").pop() + " " + " <img src=\"https://flagcdn.com/16x12/"+ artistObj.strCountryCode.toLowerCase() + ".png\" alt='flag_image'>";
    } catch (e){
        return "Lo siento, no he podido encontrar información fiable de la nacionalidad de " + artistN;
    }
}

/************************************************************************************
 *
 * @Objetivo: Método encargado de buscar la edad de un artista.
 *
 * @Parameters:
 *              (in) artistN: Artista a buscar.
 *
 * @Return:     Mensaje a mostrar
 *
 ************************************************************************************/
async function reqArtistAge(artistN){
    try{
        let artistObj = await api.getArtistInformation(artistN);
        return artistObj.strArtist + " cumple/ha cumplido este año, " + (new Date().getFullYear() - artistObj.intBornYear) + " años!";
    } catch (e){
        return "Lo siento, no he encontrado información fiable para esta pregunta";
    }
}

/************************************************************************************
 *
 * @Objetivo: Método encargado de reproducir la canción solicitada.
 *
 * @Parameters:
 *              (in) songN: Canción a reproducir.
 *
 * @Return:     Mensaje a mostrar
 *
 ************************************************************************************/
async function playSong(songN) {
    try{
        let song = await api.playSong(songN);

        switch (song) {

            case "ERROR DEVICE": {
                return "No hay ningún dispositivo conectado del que poder reproducir la canción."
            }

            case "ERROR SONG NOT FOUND": {
                return "No he encontrado la canción que intentas reproducir."
            }

            case "DEVICE NOT ACTIVE": {
                return "No hay ningún dispositivo activo donde reproducir la canción. Prueba activarlo reproduciendo alguna canción";
            }

            default: {
                return `Diselo Reina! Vamos a reproducir: ` + `<p class="album-info-title-font">${song.getName()} de ${song.getAlbum().getSinger().getName()}</p>`;
            }
        }
    } catch (e){
        return "Lo siento, no he podido reproducir la canción. Esta funcionalidad solo está disponible si eres usuario Premium de Spotify!";
    }
}

/************************************************************************************
 *
 * @Objetivo: Método encargado de reproducir la canción solicitada según el id.
 *
 * @Parameters:
 *              (in) songID: ID de la canción a reproducir.
 *
 * @Return:     Mensaje a mostrar
 *
 ************************************************************************************/
export async function playSongID(songID) {

    try{
        await api.playSongID(songID);
    } catch (e){
        console.log("Esta funcionalidad solo está disponible si eres usuario Premium de Spotify!");
    }
}

/************************************************************************************
 *
 * @Objetivo: Método encargado de reproducir la siguiente o la canción anterior.
 *
 * @Parameters:
 *              (in) songN: Canción a reproducir.
 *
 * @Return:     Mensaje a mostrar
 *
 ************************************************************************************/
async function nextOrBeforeSong(nextOrBefore) {

    try{
        let status;
        if(nextOrBefore === NEXT_SONG) {
            status = await api.nextOrBeforeSong(nextOrBefore);
        } else {
            status = await api.nextOrBeforeSong(nextOrBefore);
        }

        switch (status) {

            case "ERROR DEVICE": {
                return "No hay ningún dispositivo conectado del que poder reproducir la canción."
            }

            default: {
                return `Reproduciendo la canción: ` + `<p class="album-info-title-font">${status}</p>`;
            }
        }
    } catch (e){
        return "Lo siento, no he podido reproducir la siguiente canción.";
    }
}

/************************************************************************************
 *
 * @Objetivo: Método encargado de mostrar texto de ayuda con preguntas posibles a realizar al bot.
 *
 * @Parameters: ---
 *
 *
 * @Return:     Mensaje a mostrar
 *
 ************************************************************************************/
function getHelpCommand() {
    return `
            Te doy unos ejemplos de preguntas que me puedes hacer:
                    <br><br>
                    <p class="black-text">Recuerda:</p> Si haces algunas faltas de ortografía, trataré de enterte, no te preocupes.😛
                    <br><br>
                    Te doy unos ejemplos de preguntas que me puedes hacer:
                    <br><br>
                    <p class="black-text">Canciones:</p>
                    1. ¿Cuando salió la canción 'X'?<br>
                    2. ¿Cuanto dura la canción 'X'?<br>
                    3. Letra de 'X' / Lyrics de 'X'<br>
                    4. Recomiéndame una canción de 'género/artista' + (del año 'X') ó (entre 'año' y 'año')<br>
                    <br>
                    <p class="black-text">Ábumes:</p>
                    1. ¿Me puedes dar informacion del álbum 'X'?<br>
                    2. ¿De quién es el album 'X'?<br>
                    3. ¿Cuando salió el álbum 'X'?<br>
                    4. ¿Cuanto dura el álbum 'X'?<br>
                    5. ¿Qué canciones tiene el album 'X'?<br>                      
                    <br>
                    <p class="black-text">Artistas:</p>
                    1. ¿De donde es 'X'?<br>
                    2. ¿Qué albumes tiene 'X'?<br>
                    3. ¿Cuántos años tiene 'X'?<br>
                    4. ¿Qué artistas están relacionados con / son simlares a 'X'?  XXX
                    <br>
                    <br>
                    <p class="black-text">PlayLists:</p>
                    1. ¿Que PlayLists tengo?<br>
                    2. ¿Puedes añadir la canción 'X' a mi playlist 'X'?<br>
                    <br>
                    <p class="black-text">Reproductor de canciones:</p>
                    1. Siguiente canción / Next song...<br>
                    2. Canción de antes / Before song...<br>
                    3. Reproduce 'X' / Play 'X'...<br>
                    <br><br>
                     <p class="black-text">Puedes escribir 'ayuda' y te volveré a dar ejemplos de preguntas. 🙃</p>
            `;
}

/************************************************************************************
 *
 * @Objetivo: Método encargado de mostrar el lyrics de la canción solicitada.
 *
 * @Parameters:
 *              (in) songN: Canción de la que saber su lyrics.
 *
 * @Return:     Mensaje a mostrar
 *
 ************************************************************************************/
async function requestLyrics(songN) {

    return $.ajax({
        url: "/lyrics",
        type: 'POST',
        data: {
            "song": songN
        },
        success: function (response) {
        },
        error: function () {
        }
    })
}


// FORMA ANTES DE RECONOCER FRASES
/**
 * Método encargado de encontrar una posible respuesta para el bot.
 *
 * @param {string} message La pregunta que ha introducido el usuario.
 * @return ---
 */
export async function checkMatch(message) {

    let originalMessage = message;
    // Pasamos la pregunta a lower case.
    message = message.toLowerCase();
    // Reemplazamos carácteres espciales excepto espacios.
    message = message.replace(/[&\/\\#,+()$~%.'":*<>{}!¡¿?]/g, '');
    message = message.normalize("NFD").replace(/[\u0300-\u036f]/g, "")

    const match = stringSimilarity.findBestMatch(message, USR_MSGS);
    const matchPercentage = stringSimilarity.compareTwoStrings(message, match.bestMatch.target);
    console.log(`Match percentage: ${matchPercentage}`);

    // Miramos si existe alguna pregunta similar en nuestra BBDD.
    if (match.bestMatch.rating >= 0.4) {

        // Mirar map1_basic_questions.
        let matchIndex = map1_basics_questions.indexOf(match.bestMatch.target);
        if (matchIndex >= 0) {
            const r = random(0, map1_basics_answers.length - 1);
            return map1_basics_answers[r];
        }

        // Mirar map2_album_questions
        matchIndex = map2_album_questions.indexOf(match.bestMatch.target);
        if (matchIndex >= 0) {
            return checkAlbumMap(matchIndex, message);
        }

        // Mirar map3_playlists_questions
        matchIndex = map3_playlists_questions.indexOf(match.bestMatch.target);
        if (matchIndex >= 0) {
            return checkPlayListMap(matchIndex, message);
        }

        // Mirar map4_singers_questions
        matchIndex = map4_singers_questions.indexOf(match.bestMatch.target);
        if (matchIndex >= 0){
            return await checkSingersMap(matchIndex, originalMessage, message);
        }

        matchIndex = map5_bot_commands.indexOf(match.bestMatch.target);
        if (matchIndex >= 0){
            switch(matchIndex) {
                case 0: return `
                Te doy unos ejemplos de preguntas que me puedes hacer:
                        <br><br>
                        <p class="black-text">Recuerda:</p> Si haces algunas faltas de ortografía, trataré de enterte, no te preocupes.😛
                        <br><br>
                        Te doy unos ejemplos de preguntas que me puedes hacer:
                        <br><br>
                        <p class="black-text">Ábumes:</p>
                        1. ¿Me puedes dar informacion del álbum 'X'?<br>
                        2. ¿De quién es el album 'X'?<br>
                        3. ¿Cuando salió el álbum 'X'?<br>
                        4. ¿Cuanto dura el álbum 'X'?<br>
                        5. ¿Qué canciones tiene el album 'X'?<br>
                        <br>
                        <p class="black-text">Artistas:</p>
                        1. ¿De donde es 'X'?<br>
                        2. ¿Qué albumes tiene 'X'?<br>
                        3. ¿Con quien ha colaborado 'X'?<br>
                        4. ¿Cuántos años tiene 'X'?<br>
                        5. ¿Qué artistas están relacionados con 'X'?<br>
                        <br>
                        <p class="black-text">PlayLists:</p>
                        1. ¿Que PlayLists tengo?<br>
                        2. ¿Puedes añadir la canción 'X' a mi playlist 'X'?<br>
                        <br><br>
                         <p class="black-text">Puedes escribir 'ayuda' y te volveré a dar ejemplos de preguntas. 🙃</p>
                `;
            }
        }


    } else {
        const r = random(0, BOT_UNKNOWN_RESPONSE.length - 1);
        return BOT_UNKNOWN_RESPONSE[r];
    }
}

//Utils

/**
 * Método encargado de calcular la duración de un álbum.
 *
 *  @param {Album} album Álbum del que extraer la información.
 *  @return {string} Duración del álbum en formato min:seg
 *
 */
function getAlbumDuration(album) {

    let duration = 0;
    album.getSongs().forEach(song => {
        duration += song.getDuration();
    });
    return getDuration(duration);
}

/**
 * Método encargado de convertir la duración en milisegundos a formato min:seg
 *
 *  @param {int} msDuration Duración en milisegundos.
 *  @return {string} Conversión de duración a min:seg
 *
 */
function getDuration(msDuration) {
    let min = Math.floor((msDuration/1000/60) << 0);
    let seg = Math.floor((msDuration/1000) % 60);

    if(seg < 10) {
        return min + ':0' + seg;
    } else {
        return min + ':' + seg;
    }
}

/************************************************************************************
 *
 * @Objetivo: Método encargado de generar un número aleatorio entre un
 *            mínimo y un máximo.
 *
 * @Parameters:
 *              (in) min: Nr mínimo.
 *              (in) max: Nr máximo.
 *
 * @Return:     ---
 *
************************************************************************************/
function random(min, max) {
    return Math.floor(Math.random() * (max - min) + min);
}


// Parsear datos de la respuesta por parte de DialogFlow.
function getResponseQueryType(response) {
    if(response[0]['fields']['queryType'] !== undefined) {
        return response[0]['fields']['queryType']['stringValue'];
    } else {
        return false;
    }
}

function getResponseValue(response,type,value) {
    if(response[0]['fields'][value] !== undefined) {
        return response[0]['fields'][value][type];
    } else {
        return false;
    }
}