import * as apiFile from "./utils/spotify-web-api.js";
import {Singer} from "./model/Singer.js";
import {Album} from "./model/Album.js";
import {Song} from "./model/Song.js";
import {PlayList} from "./model/PlayList.js";
import {User} from "./model/User.js";
import * as stringSimilarity from "./utils/string-similarity.js";
import {myAccessToken} from "./index.js";


/**
 * @class Api
 * @description Clase que representa el acceso a la API de Spotify
 */

var Spotify = apiFile.SpotifyWebApi;
var spotifyApi = new Spotify();


// General function.
function matchPorcentaje(string1, string2) {

    string1 = string1.toLowerCase();
    string1 = string1.replace(/[&\/\\#,+()$~%.'":*<>{}!¡¿?]/g, '');
    string1 = string1.normalize("NFD").replace(/[\u0300-\u036f]/g, "")

    string2 = string2.toLowerCase();
    string2 = string2.replace(/[&\/\\#,+()$~%.'":*<>{}!¡¿?]/g, '');
    string2 = string2.normalize("NFD").replace(/[\u0300-\u036f]/g, "")
    return stringSimilarity.compareTwoStrings(string1, string2)
}

async function fillData(songData) {
    let song = new Song();
    let album = new Album();
    let singer = new Singer();

    song.setID(songData.id);
    song.setDuration(songData.duration_ms);
    song.setTrackNumber(songData.track_number);
    song.setName(songData.name);
    album.setSpotifyID(songData.album.id);
    album.setName(songData.album.name);
    album.setReleaseDate(songData.album.release_date);
    singer.setSpotifyID(songData.album.artists[0].id);
    singer.setName(songData.album.artists[0].name);
    album.setSinger(singer);
    song.setAlbum(album);
    song.setTrackNumber(songData.track_number);
    return song;
}

async function reconoceGenero(genre) {

    let data = await spotifyApi.getAvailableGenreSeeds();
    let bestMatch = 0;
    let generoReconocido;

    console.log(data.genres);

    for(let i = 0; i < data.genres.length; i++) {
        let actualMatch = await matchPorcentaje(data.genres[i],genre);
        if(actualMatch > bestMatch) {
            bestMatch = actualMatch;
            generoReconocido = data.genres[i];
        }
    }

    return generoReconocido;
}


// Spotify Functions

/**
 * Método encargado de recomendar la canción de un género a partir de su nombre y fecha de lanzamiento.
 *
 * @param {string} genre Género en cuestión.
 * @param {string} firstYear Primer año del que filtrar.
 * @param {string} finalYear Segundo año del que filtrar ó único año del que filtrar.
 *
 * @return canción recomendada según los parámetros descritos.
 */
export async function recommendSongGenre(genre, firstYear, finalYear) {

    spotifyApi.setAccessToken(myAccessToken);

    let data;
    let opcion;
    let generoReconocido = await reconoceGenero(genre);

    if(firstYear != undefined && finalYear != undefined) {
        opcion = 1;

        if(finalYear < firstYear) {
            let aux = finalYear;
            finalYear = firstYear;
            firstYear = aux;
        }
        data = await searchFromSpotify("genre:"+generoReconocido+", year:"+firstYear+"-"+finalYear, "track",50);

        if(data.tracks.items.length == 0) {
            return "Error";
        }
        if(getYearFromRelease(data.tracks.items[0].album.release_date) > finalYear) {
            return "Error";
        }

        // Recomendamos canción random de un genero.
    } else if (firstYear == undefined && finalYear == undefined) {
        opcion = 2;
        data = await searchFromSpotify("genre:"+generoReconocido, "track",50);
        // Recomendamos canción de un genero de un año.
    } else {
        opcion = 3;
        data = await searchFromSpotify("genre:"+generoReconocido+ ", year:"+finalYear+"-"+finalYear, "track",50);

        if(data.tracks.items.length == 0) {
            return "Error";
        }

        if(getYearFromRelease(data.tracks.items[0].album.release_date) != finalYear) {
            return "Error";
        }
    }

    let songs = [];
    let songData;
    let numSongs = 0;
    switch (opcion) {
        case 1:

            for(let i = 0; i < data.tracks.items.length; i++) {
                let year = getYearFromRelease(data.tracks.items[i].album.release_date)
                if(year <= finalYear && year >= firstYear) {
                    songs[numSongs] = data.tracks.items[i];
                    numSongs++;
                }
            }
            break;

        case 2:
            for(let i = 0; i < data.tracks.items.length; i++) {
                songs[numSongs] = data.tracks.items[i];
                numSongs++;
            }
            break;

        case 3:
            for(let i = 0; i < data.tracks.items.length; i++) {
                let year = getYearFromRelease(data.tracks.items[i].album.release_date)
                if(year == finalYear) {
                    songs[numSongs] = data.tracks.items[i];
                    numSongs++;
                }
            }
            break;
    }
    if(numSongs > 0) {
        songData = songs[generateRandom(0,numSongs-1)];
        let songParsed = await fillData(songData);
        return songParsed;
    } else {
        return "Error";
    }

}

/**
 * Método encargado de recomendar la canción de un artista a partir de su nombre y fecha de lanzamiento.
 *
 * @param {string} artist Nombre del artista en cuestión.
 * @param {string} firstYear Primer año del que filtrar.
 * @param {string} finalYear Segundo año del que filtrar ó único año del que filtrar.
 *
 * @return canción recomendada según los parámetros descritos.
 */
export async function recommendSongArtist(artist,firstYear,finalYear) {
    spotifyApi.setAccessToken(myAccessToken);
    let data;
    let opcion;

    console.log("patata");

    if(artist == undefined) {
        return "Error";
    }

    // Recomendamos canción de artista entre 2 años.
    if(firstYear != undefined && finalYear != undefined) {
        opcion = 1;

        if(finalYear < firstYear) {
            let aux = finalYear;
            finalYear = firstYear;
            firstYear = aux;
        }

        data = await searchFromSpotify("artist:"+artist+", year:"+firstYear+"-"+finalYear, "track",50);

        if(data.tracks.items.length == 0) {
            return "Error";
        }

        if(getYearFromRelease(data.tracks.items[0].album.release_date) > finalYear) {
            return "Error";
        }

        // Recomendamos canción random de un artista.
    } else if (firstYear == undefined && finalYear == undefined) {
        opcion = 2;
        data = await searchFromSpotify("artist:"+artist, "track",50);
        // Recomendamos canción de artista de un año.
    } else {
        opcion = 3;
        data = await searchFromSpotify("artist:"+artist+ ", year:"+finalYear+"-"+finalYear, "track",50);

        if(data.tracks.items.length == 0) {
            return "Error";
        }
        if(getYearFromRelease(data.tracks.items[0].album.release_date) != finalYear) {
            return "Error";
        }
    }

    //console.log(data);
    let songs = [];
    let songData;
    let numSongs = 0;
    switch (opcion) {
        case 1:

            for(let i = 0; i < data.tracks.items.length; i++) {
                let year = getYearFromRelease(data.tracks.items[i].album.release_date)
                const matchPercentage = matchPorcentaje(data.tracks.items[i].album.artists[0].name, artist);
                if(year <= finalYear && year >= firstYear && matchPercentage >= 0.4) {
                    songs[numSongs] = data.tracks.items[i];
                    numSongs++;
                }
            }
            break;

        case 2:
            for(let i = 0; i < data.tracks.items.length; i++) {
                const matchPercentage = matchPorcentaje(data.tracks.items[i].album.artists[0].name, artist);
                if(matchPercentage >= 0.4) {
                    songs[numSongs] = data.tracks.items[i];
                    numSongs++;
                }
            }
            break;

        case 3:
            for(let i = 0; i < data.tracks.items.length; i++) {
                const matchPercentage = matchPorcentaje(data.tracks.items[i].album.artists[0].name, artist);
                let year = getYearFromRelease(data.tracks.items[i].album.release_date)
                if(year == finalYear && matchPercentage >= 0.4) {
                    songs[numSongs] = data.tracks.items[i];
                    numSongs++;
                }
            }
            break;
    }
    if(numSongs > 0) {
        songData = songs[generateRandom(0,numSongs-1)];
        let songParsed = await fillData(songData);
        return songParsed;
    } else {
        return "Error";
    }
}

/**
 * Método encargado de obtener la información de un Artista según su nombre.
 *
 * @param {string} artistName Nombre del artista en cuestión.
 * @return res Resultados obtenidos desde la api theaudiodb.
 */
export async function getArtistInformation(artistName) {

    artistName = artistName.replace(" ","%20");
    const url = 'https://www.theaudiodb.com/api/v1/json/2/search.php?s=' + artistName;
    try {
        let res = await fetch(url);
        let json = await res.json()
        return json.artists[0];
    } catch (error) {
        console.log(error);
    }
}

/**
 * Método encargado de buscar información sobre un álbum.
 *
 * @param {string} albumName Album del que encontrar información.
 * @return album/error Clase album llena de información/Error en caso de no encontrar el album.
 */
export async function searchAlbum(albumName) {

    spotifyApi.setAccessToken(myAccessToken);
    let album = new Album();
    let singer = new Singer();
    let data = await spotifyApi.searchAlbums(albumName, {limit: 1});

    // Obtenemos información del álbum.

    if(data.albums.items.length > 0) {

        let albumData = data.albums.items[0];

        // Recorremos información del artista del álbum.
        albumData.artists.forEach(artist => {
                singer.setName(artist.name);
                singer.setSpotifyID(artist.id);
            }
        )

        let dataArtist = await spotifyApi.getArtist(singer.getSpotifyID(),{limit:1});
        singer.setGenres(dataArtist.genres);


        // Llenamos información del álbum.
        album.setSinger(singer);
        album.setName(albumData.name);
        album.setReleaseDate(albumData.release_date);
        album.setTotalTracks(albumData.total_tracks);
        album.setSpotifyID(albumData.id);
        album.setImage(albumData.images[0].url);

        // Almacenamos información de las canciones del álbum.
        let allSongs = [];
        let dataSongs = await spotifyApi.getAlbum(albumData.id, {limit: 1});
        let tracks = dataSongs.tracks.items;

        for(let i = 0; i < tracks.length; i++) {
            let track = tracks[i];
            let trackData = await spotifyApi.getTrack(track.id, {limit:1});

            // Creamos instancia de canción con sus datos correspondientes.
            let song = new Song(trackData.name, trackData.duration_ms, trackData.genre, "");
            song.setID(track.id);
            allSongs.push(song);
        }

        album.setSongs(allSongs);
        return album;

    } else {
        return "Error";
    }
}

/**
 * Método encargado de buscar información de una canción a partir de su nombre.
 *
 * @param {string} songN Nombre de la canción
 * @return album/error Clase album llena de información/Error en caso de no encontrar el album.
 */
export async function searchSong(songN) {

    spotifyApi.setAccessToken(myAccessToken);
    let data = await spotifyApi.searchTracks(songN, {limit: 1});

    // Obtenemos información del álbum.

    if(data.tracks.items.length > 0) {

        let songData = data.tracks.items[0];

        let song = new Song();
        let album = new Album();
        let singer = new Singer();

        //let dataAlbum = await spotifyApi.searchAlbums(songDat)

        song.setID(songData.id);
        song.setDuration(songData.duration_ms);
        song.setTrackNumber(songData.track_number);
        song.setName(songData.name);
        album.setSpotifyID(songData.album.id);
        album.setName(songData.album.name);
        album.setReleaseDate(songData.album.release_date);
        singer.setSpotifyID(songData.album.artists[0].id);
        singer.setName(songData.album.artists[0].name);
        album.setSinger(singer);
        song.setAlbum(album);
        song.setTrackNumber(songData.track_number);

        return song;
    } else {
        return "Error";
    }
}

/**
 * Método encargado de encontrar la lista de PlayLists del usuario logueado.
 *
 * @param ---
 * @return playlists/error Lista de PlayLists del usuario/Error en caso de no tener PlayLists creadas.
 */
export async function searchMyPlayLists() {

    spotifyApi.setAccessToken(myAccessToken);
    let user = await getMyUsername();
    let playlists = [];
    let data = await spotifyApi.getUserPlaylists(user.getID(),{limit:50});
    if(data.items.length > 0) {
        for (const item of data.items) {
            if(item.collaborative || item.owner.id === user.getID()) {
                let songs = [];
                let playlist = new PlayList();
                playlist.setName(item.name);
                playlist.setImage(item.images[0].url);
                playlist.setID(item.id);
                /*
                let dataSongs = await spotifyApi.getPlaylistTracks(playlist.id);
                dataSongs.items.forEach(item => {
                    let song = new Song();
                    let album = new Album();
                    song.setName(item.track.name);
                    song.setDuration(item.track.duration_ms);
                    song.setID(item.track.id);
                    album.setName(item.track.album.name);
                    album.setSpotifyID(item.track.album.id);
                    song.setAlbum(album);
                    songs.push(song);
                })
                playlist.setSongs(songs);
                */
                playlists.push(playlist);
            }
        }
        return playlists;
    } else {
        return "Error";
    }
}

/**
 * Método encargado de añadir una canción a una PlayList del usuario logueado.
 *
 * @param songName Nombre de la canción a añadir.
 * @param playListName Nombre de la playlist donde añadir la canción.
 * @return Mensaje conforme se ha podido añadir la canción exitosamente o error en cuestión.
 */
export async function addSongToMyPlayList(songName,playListName) {

    spotifyApi.setAccessToken(myAccessToken);
    let dataTracks = await spotifyApi.searchTracks(songName,{limit: 1});
    if(dataTracks.tracks.items.length > 0) {

        let song = new Song();
        song.setName(dataTracks.tracks.items[0].name);
        song.setID(dataTracks.tracks.items[0].id);

        let encontrado = false;
        let playlists = await searchMyPlayLists();
        let maxMatch = 0;
        let indexBestMatch = 0;

        for(let i = 0; i < playlists.length && !encontrado; i++) {
            let match = stringSimilarity.compareTwoStrings(playlists[i].getName(), playListName);
            if(match > maxMatch) {
                maxMatch = match;
                indexBestMatch = i;
            }
        }

        let playlist = playlists[indexBestMatch];

        if(maxMatch > 0.6) {
            let songToAdd = [];
            songToAdd[0] = "spotify:track:"+song.getID();
            await spotifyApi.addTracksToPlaylist(playlist.getID(),songToAdd);
            return "La canción " + song.getName() + " se ha añadido a la lista '"
                + playlist.getName() + "' correctamente!";
        } else {
            return "Lo siento, no he podido encontrar la Playlist que me has pedido.";
        }
    } else {
        return "Lo siento, no he podido encontrar la canción que me has pedido";
    }
}

/**
 * Método encargado de realizar una petición de search.
 *
 * @param item El nombre de lo que deseamos buscar.
 * @param type El tipo de cosa de la que deseamos buscar información. artist, track, album...
 * @return Objeto con la información encontrada al respecto.
 */
export async function searchFromSpotify(item, type, limit){
    spotifyApi.setAccessToken(myAccessToken);
    return spotifyApi.search(item.trim(), [type.trim()], {limit: limit});
}

/**
 * Método encargado de obtenes los artistas relacionados con un artista.
 *
 * @param item Artista del que queremos buscar otros artistas relacionados.
 * @return Objeto con la información de los artistas relacionados.
 */
export async function getArtistsRelatedToArtist(artist){
    let artistInfo = await searchFromSpotify(artist, "artist",1);

    let dataSplit = artistInfo.artists.items[0].uri.split(":");
    let artistID = dataSplit[2];
    let artistsInfo = await spotifyApi.getArtistRelatedArtists(artistID, {limit: 20});
    let artistsList = [];
    artistsInfo.artists.forEach(albLoop);
    function albLoop(e) {
        var singer = new Singer();

        singer.setImg(e.images[0].url);
        singer.setName(e.name);
        singer.setSpotifyID(e.id);

        artistsList.push(singer);
    }
    return artistsList;
}

/**
 * Método encargado de obtener los álbumes de un artista.
 *
 * @param item El nombre del artista.
 * @param limit El límite de datos que deseamos obtener (max 50).
 * @return Objeto con los álbumes encontrados.
 */
export async function getAlbumsFromArtist(artist, limit){

    try {
        spotifyApi.setAccessToken(myAccessToken);
        let artistInfo = await searchFromSpotify(artist, "artist",1);
        let dataSplit = artistInfo.artists.items[0].uri.split(":");
        let artistID = dataSplit[2];

        let data = await spotifyApi.getArtistAlbums(artistID, {limit: limit});
        let albumsList = [];
        var i = 0;
        data.items.forEach(feLoop)
        function feLoop(e) {
            var singer = new Singer();
            var album = new Album();
            e.artists.forEach(artist => {
                    singer.setName(artist.name);
                    singer.setSpotifyID(artist.id);
                }
            )

            // Llenamos información del álbum.
            album.setSinger(singer);
            album.setName(e.name);
            album.setReleaseDate(e.release_date);
            album.setTotalTracks(e.total_tracks);
            album.setSpotifyID(e.id);
            album.setImage(e.images[0].url);
            albumsList.push(album);
            i++;
        }
        return albumsList;

    }catch (e) {
        console.log("Error");
        return e;
    }
}

/**
 * Método encargado de obtener información del usuario conectado al ChatBot.
 *
 * @return Objeto con la información de mi usuario.
 */
export async function getMyUsername() {
    spotifyApi.setAccessToken(myAccessToken);
    let myUsername = await spotifyApi.getMe({limit: 1});
    return new User(myUsername.id,myUsername.display_name,myUsername.images[0].url);
}

/**
 * Método encargado reproducir una canción
 *
 * @param songN El nombre de la canción que se desea reproducir.
 *
 * @return Se reproduce la canción seleccionada.
 */
export async function playSong(songN) {

    spotifyApi.setAccessToken(myAccessToken);

    // Obtenemos los dispositivos conectados a la cuenta.
    let answer = await spotifyApi.getMyDevices();
    let actualDevices = [];
    let i = 0;

    answer.devices.forEach(device => {
        actualDevices[i] = device;
        i++;
    })

    if(actualDevices.length == 0) {
        return "ERROR DEVICE";
    } else {

        if(actualDevices[0].is_active) {
            let song = await searchSong(songN);

            if(song != "Error") {
                await spotifyApi.play({
                    context_uri: "spotify:album:"+song.getAlbum().getSpotifyID(),
                    offset: {
                        position: song.getTrackNumber()-1
                    }
                });
                 return song;
            } else {
                return "ERROR SONG NOT FOUND";
            }
        } else {
            return "DEVICE NOT ACTIVE"
        }
    }
}

/**
 * Método encargado reproducir una canción según su ID
 *
 * @param songID El id de la canción que se desea reproducir.
 *
 * @return Se reproduce la canción seleccionada.
 */
export async function playSongID(songID) {

    spotifyApi.setAccessToken(myAccessToken);

    // Obtenemos los dispositivos conectados a la cuenta.
    let answer = await spotifyApi.getMyDevices();
    let actualDevices = [];
    let i = 0;

    answer.devices.forEach(device => {
        actualDevices[i] = device;
        i++;
    })

    if(actualDevices.length == 0) {
        console.log("DEVICE NOT FOUND");
    } else {

        if(actualDevices[0].is_active) {
            let song = await spotifyApi.getTrack(songID);

            if(song != "Error") {
                await spotifyApi.play({
                    context_uri: "spotify:album:"+song.album.id,
                    offset: {
                        position: song.track_number-1
                    }
                });
            } else {
                console.log("ERROR SONG NOT FOUND");
            }
        } else {
            console.log("DEVICE NOT ACTIVE");
        }
    }
}

/**
 * Método de obtener la canción y el artista de una canción.
 *
 * @param songN El nombre de la canción en concreto.
 *
 * @return Song + Artist
 */
export async function getSongAndArtist(songN) {

    spotifyApi.setAccessToken(myAccessToken);
    let dataTracks = await spotifyApi.searchTracks(songN,{limit: 1});

    if(dataTracks.tracks.items.length > 0) {
        return [dataTracks.tracks.items[0].name, dataTracks.tracks.items[0].artists[0].name];
    } else {
        return "ERROR";
    }
}

/**
 * Método encargado de saltar a la canción siguiente o a la anterior en la cola/lista
 *
 * @param nextOrBefore Siguiente canción o la de antes.
 *
 * @return Se reproduce la canción seleccionada.
 */
export async function nextOrBeforeSong(nextOrBefore) {

    spotifyApi.setAccessToken(myAccessToken);

    // Obtenemos los dispositivos conectados a la cuenta.
    let answer = await spotifyApi.getMyDevices();
    let actualDevices = [];
    let i = 0;

    answer.devices.forEach(device => {
        actualDevices[i] = device;
        i++;
    })

    if(actualDevices.length == 0) {
        return "ERROR DEVICE";
    } else {
        if (nextOrBefore != "nextSong") {
            await spotifyApi.skipToNext();
        } else {
            await spotifyApi.skipToPrevious();
        }

        let actualSong = await spotifyApi.getMyCurrentPlayingTrack();
        return actualSong.item.name;

    }
}

/**
 * Método eencargado de obtener el año de las fechas de tipo "release_date" de la api de Spotify.
 *
 * @param songID El id de la canción que se desea reproducir.
 *
 * @return Se reproduce la canción seleccionada.
 */
function getYearFromRelease(releaseDate) {
    return releaseDate.substring(0,4);
}

/**
 * Método encargado de generar un número aleatorio entre un máximo y un mínimo
 *
 * @param min El nr mínimo
 * @param max El nr máximo
 *
 * @return Nr aleatorio.
 */
function generateRandom(min, max) {
    let difference = max - min;
    let rand = Math.random();
    rand = Math.floor( rand * difference);
    rand = rand + min;
    return rand;
}