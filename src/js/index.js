import * as botFile from './bot.js';
import * as api from './api.js';


const msgerForm = get(".chat-inputarea"); // Formulario con input + button.
const msgerInput = get(".chat-input");    // Input del mensaje
const msgerChat = get(".window-chat");    // Chat en general

// Constantes
const BOT_IMG = "https://www.svgrepo.com/show/299429/spotify.svg";
const PERSON_IMG = "https://www.svgrepo.com/show/7892/user.svg";
const BOT_NAME = "PEPI";

const WAITING_RESPONSE = `<div class="wrapper">
                            <span class="circle circle-1"></span>
                            <span class="circle circle-2"></span>
                            <span class="circle circle-3"></span>
                            <span class="circle circle-4"></span>
                        </div>`

let myUser;
let loaded = false;
export let myAccessToken;
function findMyToken(){
    let window = document.location.href;
    myAccessToken = window.split("?").pop();
}


/**
 * Método de gestionar el submit por parte del usuario.
 *
 */
msgerForm.addEventListener("submit", async event => {
    event.preventDefault();

    findMyToken();                              // Obtenemos token.

    if(!loaded) {
        myUser = await api.getMyUsername();     // Obtenemos usuario solo en el primer mensaje que se solicite por el usuario.
        loaded = true;
    }
    const msgText = msgerInput.value;           // Guardamos texto introducido.
    if (!msgText) return;                       // Comprobamos si se ha introducido texto.

    // Mostramos mensaje del usuario.
    appendMessage(myUser.getDisplayName(), myUser.getImage(), "right", msgText);   // Append del mensaje del usuario.
    msgerInput.value = "";                                           // Limpiamos input de mensaje.

    await getDialogFlowResponse(msgText);
});

/************************************************************************************
 *
 * @Objetivo: Mostrar un mensaje de algún usuario por pantalla.
 *
 * @Parameters:
 *              (in) name: Nombre del usuario en cuestión.
 *              (in) img:  Imágen a mostrar del usuario.
 *              (in) side: right: Mensaje a la derecha.
 *                         left:  Mensaje a la izquierda.
 *              (in) text: Texto del mensaje.
 * 
 * @Return:     ---
 *
************************************************************************************/
function appendMessage(name, img, side, text) {
    const msgHTML = `
    <div class="msg ${side}-msg">   
      <div class="msg-img" style="background-image: url(${img})"></div>

      <div class="msg-bubble">
        <div class="msg-info">
          <div class="msg-info-name">${name}</div>
          <div class="msg-info-time">${formatDate(new Date())}</div>
        </div>

        <div class="msg-text">${text}</div>
      </div>
    </div>
  `;
    msgerChat.insertAdjacentHTML("beforeend", msgHTML);
    msgerChat.scrollTop += 500;
}

/************************************************************************************
 *
 * @Objetivo: Se encarga de obtener una respuesta por parte del bot.
 *
 * @Parameters:
 *              (in) response: Mensaje a escribir por parte del bot.
 *
 * @Return:     ---
 *
 ************************************************************************************/
function botResponse(response, status) {

    // Miramos si se ha escrito una respuesta para así eliminar el mensaje de "..." anterior.
    if(status === "RESPONSE") {
        let bubbles = document.getElementsByClassName("msg left-msg");
        bubbles[bubbles.length-1].remove();
    }
    appendMessage(BOT_NAME, BOT_IMG, "left", response);
}

/**
 *
 * @Objetivo: Se encarga de obtener una respuesta desde la API de DialogFlor que procesaremos para realizar
 *            la búsqueda de información en las otras fuentes de información como es la API de Spotify.
 *
 *
 * @Parameters:
 *              (in) text: Mensaje introducido por el usuario.
 *
 * @Return:     ---
 */
async function getDialogFlowResponse(text) {

    $.ajax({
        url: "/dialogflow",
        type: 'POST',
        data: {
            "text": text
        },
        success: async function (response) {
            botResponse(WAITING_RESPONSE, "WAITING");
            const textResponse = await botFile.getResponse(response);
            botResponse(textResponse, "RESPONSE");
            addOnClicksPlaySong();

        },
        error: function () {
            console.log("FATAL!");
        }
    })
}

/*
const apiButton = get(".api-button");    // Chat en general
apiButton.addEventListener("click", async event => {
    findMyToken();
    let patata = await api.getMyUsername();
    console.log(patata);
});
*/


// Utils
function addOnClicksPlaySong() {
    let playSongsButtons = document.getElementsByClassName("playButton");    // Chat en general
    for(var i = 0; i < playSongsButtons.length; i++) {

        let element = document.getElementById(playSongsButtons[i].id);
        element.addEventListener("click", async event => {
            event.preventDefault();
            await botFile.playSongID(element.id)
        });
    }
}

function get(selector, root = document) {
    return root.querySelector(selector);
}

function formatDate(date) {
    const h = "0" + date.getHours();
    const m = "0" + date.getMinutes();

    return `${h.slice(-2)}:${m.slice(-2)}`;
}

function random(min, max) {
    return Math.floor(Math.random() * (max - min) + min);
}



