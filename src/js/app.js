var SpotifyWebApi = require('spotify-web-api-node');
const express = require('express')
const app = express();
const path = require('path');
const fetchCheerioObject = require('fetch-cheerio-object');
const bodyParser = require('body-parser');

app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json());


// Genius Lyrics
const Genius = require("genius-lyrics");
const Client = new Genius.Client();

// DialogFlow
const config = require('./utils/devkey');
const dialogflow = require('@google-cloud/dialogflow');
const uuid = require('uuid');

// Vars
let session_started = false;
let expires_in;
let myAccessToken;

// Credenciales del usuario Spotify.
const CLIENT_ID = '67435237fb534167877644d39afaa5d0';
const CLIENT_SECRET = "cae8194967b74dba849417d27f8b2eea";

// Permisos que le damos a nuestro token para realizar X acciones sobre la API.
const scopes = [
    'ugc-image-upload',
    'user-read-playback-state',
    'user-modify-playback-state',
    'user-read-currently-playing',
    'streaming',
    'app-remote-control',
    'user-read-email',
    'user-read-private',
    'playlist-read-collaborative',
    'playlist-modify-public',
    'playlist-read-private',
    'playlist-modify-private',
    'user-library-modify',
    'user-library-read',
    'user-top-read',
    'user-read-playback-position',
    'user-read-recently-played',
    'user-follow-read',
    'user-follow-modify'
];

// Seteamos credenciales.
var spotifyApi = new SpotifyWebApi({
    clientId: CLIENT_ID,
    clientSecret: CLIENT_SECRET,
    redirectUri: 'http://localhost:8888/callback'
});

// Create a new session for DialogFlow
const sessionId = uuid.v4();
const sessionClient = new dialogflow.SessionsClient();
const sessionPath = sessionClient.projectAgentSessionPath(
    config.googleProjectID,
    sessionId
);

// Método encargado de realizar una petición a la API de DialogFlow con nuestro NLP.
async function runSample(texto) {

    // Petición a realizar a la API de DialogFlow.
    const request = {
        session: sessionPath,
        queryInput: {
            text: {
                // The query to send to the dialogflow agent
                text: texto,
                // The language used by the client (en-US)
                languageCode: 'es-ES',
            },
        },
    };

    // Enviar request.
    const responses = await sessionClient.detectIntent(request);
    const result = responses[0].queryResult;
     return result;
}

// function to get the raw data
const getRawData = (URL) => {
    return fetchCheerioObject(URL)
        .then((response) => response.text())
        .then((data) => {
            return data;
        });
};

app.get('/',(req,res) => {
    if(!session_started) {
        res.redirect('/login');
    } else {
        res.redirect('/chatbot?'+myAccessToken);
    }
})

app.get('/chatbot', (req,res) => {
    if(!session_started) {
        res.redirect('/login');
    } else {
        app.use(express.static('src'));
        res.sendFile(path.join(__dirname,'..','index.html'));
    }
});

app.get('/login', (req, res) => {
    res.redirect(spotifyApi.createAuthorizeURL(scopes));
});

app.get('/callback', async (req, res) => {

    const error = req.query.error;
    const code = req.query.code;
    const state = req.query.state;

    if (error) {
        console.error('Callback Error:', error);
        res.send(`Callback Error: ${error}`);
        return;
    }

    // Realizamos la petición Grant para obtener el token del usuario.
    await spotifyApi.authorizationCodeGrant(code).then(data => {
        const access_token = data.body['access_token'];
        const refresh_token = data.body['refresh_token'];
        expires_in = data.body['expires_in'];
        spotifyApi.setAccessToken(access_token);
        spotifyApi.setRefreshToken(refresh_token);
        myAccessToken = spotifyApi.getAccessToken();
        /*
        console.log(access_token:, access_token);
        console.log(refresh_token:, refresh_token);
        */

        session_started = true;                         // Marcamos la sesión como iniciada.
        res.redirect('/chatbot?'+myAccessToken);    // Redirigimos al usuario a la URL correspondiente.

    }).catch(error => {
        console.error('Error getting Tokens:', error);
        res.send(`Error getting Tokens: ${error}`);
    });
});

app.post('/dialogflow', async (req, res) => {

    // Realizamos petición a DialogFlow con la frase introducida por el usuario.
    const result = await runSample(req.body.text);
    //console.log(result.parameters);
    //console.log(result.fulfillmentText);
    const bodyResponse = [result.parameters, result.fulfillmentText];
    return res.send(bodyResponse);
});

app.post('/lyrics', async (req, res) => {

    try{
        const searches = await Client.songs.search(req.body.song);
        // Escogemos la primera canción.
        const firstSong = searches[0];
        // Obtenemos el lyrics de esta misma canción.
        const lyrics = await firstSong.lyrics();
        return res.send(lyrics);

    }catch (e) {
        return res.send("ERROR");
    }


    //return res.send(data);
});










app.listen(8888, () =>
    console.log('Servidor HTTP levantado. Visita http://localhost:8888/ en tu buscador :).')
);