# SBC Chatbot - PepiBot
> Javascript, HTML y CSS
> 
> Entornos de trabajo: PHPStorm, VisualStudio Code...

## Requerimientos
* Tener una cuenta de Spotify. [Te puedes crear una aqui!](https://www.spotify.com/es/signup)
* Puedes encontrar un tutorial con imágenes de la instalación en el mismo PowerPoint entregado en el pozo.

## Instalación

1. Abrir el proyecto con VisualStudio Code, Php Storm...
2. Instalar los `node_modules` que requieren el proyecto escribiendo el comando: `npm install` en el terminal dentro de la carpeta.
3. En el terminal desde el proyecto introducir el siguiente comando para poder utilizar la herramienta de NLP DialogFlow.
####MAC: 
`export GOOGLE_APPLICATION_CREDENTIALS="src/js/utils/credentials.json"`
####WINDOWS: 
`$env:GOOGLE_APPLICATION_CREDENTIALS="src/js/utils/credentials.json"`

## En caso de querer utilizar tu propia cuenta de Spotify (Recomendable si tienes Premium):
1. Realizar [login](https://developer.spotify.com/dashboard/) en la web de Spotify para Desarrolladores.
2. Crear una nueva [aplicacion](https://developer.spotify.com/dashboard/applications) con un nombre y una descripción intuitiva.
3. Acceder a la aplicación creada.
4. Seleccionar `EDIT SETTINGS`
5. Introducir en `REDIRECT URIs` la siguiente uri: http://localhost:8888/callback y guardar.
6. Copiar `Client ID` y el `Client secret` de la web e introducir su información en `app.js` dentro de las constantes:`CLIENT_ID` y `CLIENT_SECRET`

## En caso de querer utilizar la cuenta que hemos creado para hacer pruebas:
Al ejecutar el programa y entrar en https://localhost:8888/login , realizar login con las siguientes credenciales:
Nota: Al ser una cuenta NO premium, no dispone de la funcionalidad de reproducir canciones.

`E-mail: pepibotsbc@mzico.com`

`Password: pepi1234`



## Utilización:

1. Ejecutar el comando `npm run start` y acceder a la dirección: [https://localhost:8888/](https://localhost:8888/)
2. ¡Realizarle preguntas al ChatBot Pepi!




